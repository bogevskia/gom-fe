import React, {Component} from 'react';
import './App.css';
import {Route, Switch, withRouter, NavLink, Redirect} from 'react-router-dom';
import Main from './components/Main';
import Aux from './util/Aux';
import AllProducts from './components/AllProducts';
import AddProduct from './components/AddProduct';

class App extends Component {

    render() {

        let routes = (
            <Aux>
                <nav className="navbar navbar-expand-lg navbar-light bg-light fixed-top">
                    <div className="container">
                        <NavLink className="navbar-brand text-success" to="/">GOM</NavLink>
                        <button className="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                                aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"/>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarResponsive">
                            <ul className="navbar-nav ml-auto">
                                <li className="nav-item">
                                    <NavLink className="nav-link text-success" exact={true} to="/">Home
                                        <span className="sr-only">(current)</span>
                                    </NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className="nav-link text-success" exact={true} to='/add-product'>Add product</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className="nav-link text-success" exact={true} to="/all-products">All products</NavLink>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <Switch>
                    <Route path="/" exact={true} xact component={Main}/>
                    <Route path="/add-product" exact={true} component={AddProduct}/>
                    <Route path="/all-products" exact={true} component={AllProducts}/>
                    <Redirect to="/"/>
                </Switch>
                <footer className="py-5 bg-light jumbotron-fluid">
                    <div className="container">
                        <p className="m-0 text-center text-success">Copyright &copy; Global Organic Market 2019</p>
                    </div>
                </footer>
            </Aux>
        );
        return (
            <div className="App">
                <main>
                    {routes}
                </main>
            </div>
        );
    }
}

export default withRouter(App);
