import React, {Component} from 'react';
import axios from '../custom-axios';
import Aux from '../util/Aux';

class AddProduct extends Component {

    state = {
        name: '',
        producer: '',
        category: '',
        details: '',
        allCategories: [],
        image: null,
        isValid: false

    };

    componentDidMount() {
        axios.get('/category-types')
            .then(response => {
                this.setState({allCategories: response.data})
            })
    }

    changeCategory = (event) => {
        let newState = {
            ...this.state
        };

        newState.category = event.target.value;
        this.setState({...newState});
        this.validate(newState);
    };

    validate = (newState) => {
        if (newState.name !== '' && newState.producer !== '' && newState.category !== '' && newState.details !== ''
            && !!newState.image) {
            this.setState({isValid: true});
        } else {
            this.setState({isValid: false});
        }
    };

    inputChange = (event, property) => {
        let newState = {
            ...this.state
        };
        newState[property] = event.target.value;
        this.setState({...newState});
        this.validate(newState);
    };

    imageChange = (event) => {
        let newState = {
            ...this.state
        };
        newState.image = event.target.files[0];
        this.setState({...newState});
        this.validate(newState);
        console.log(newState);
    };

    onSaveProduct = () => {
        const {isValid} = this.state;
        let formData = new FormData();
        if (isValid) {
            formData.append("name", this.state.name);
            formData.append("category", this.state.category);
            formData.append("details", this.state.details);
            formData.append("producer", this.state.producer);
            formData.append("image", this.state.image);
            axios({
                url: '/products',
                method: 'POST',
                data: formData
            }).then(response => {
                console.log(response.data);
            });

            this.setState({name: '', category: '', details: '', image: null, producer: '', isValid: false});
        }
    };

    render() {
        const {name, producer, category, allCategories, isValid, details} = this.state;
        let categories = null;
        if (allCategories && allCategories.length) {
            categories = allCategories.map(category => {
                return <option value={category} key={category} name={category}>{category}</option>
            })
        }
        return (
            <Aux>
                <div className="container">
                    <h2 className="text-center text-success">Add a new product!</h2>
                    <div className="card" style={{marginBottom: '160px'}}>
                        <div className="card-header bg-success text-center text-white">Product details:</div>
                        <div className="card-body">
                            <div className="form-group">
                                <label htmlFor="name">Name:</label>
                                <input type="text" className="form-control" id="name" value={name}
                                       onChange={(event) => {
                                           this.inputChange(event, 'name')
                                       }}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="producer">Producer name:</label>
                                <input type="text" className="form-control" id="producer" value={producer}
                                       onChange={(event) => {
                                           this.inputChange(event, 'producer')
                                       }}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="category">Product category: </label>
                                <select className="form-control" name="category" id="product1" value={category}
                                        onChange={(event) => {
                                            this.changeCategory(event)
                                        }}>
                                    <option id="default" name="default">---</option>
                                    {categories}
                                </select>
                            </div>
                            <div className="form-group">
                                <label htmlFor="details">Product details:</label>
                                <input type="text" className="form-control" id="details" value={details} onChange={(event) => {
                                    this.inputChange(event, 'details')
                                }}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="image">Upload an image:</label>
                                <input type="file" className="badge-success" id="image" onChange={(event) => {
                                    this.imageChange(event)
                                }}/>
                            </div>
                        </div>
                        <div className="card-footer">
                            <div className="">
                                <button onClick={this.onSaveProduct} className="btn btn-success btn-block btn-lg"
                                        disabled={!isValid}>Save
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </Aux>
        );
    }

}

export default AddProduct;