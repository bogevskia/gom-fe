import React, {Component} from 'react';
import Aux from '../util/Aux';
import axios from "../custom-axios";
import Organic from '../assets/organicc.png';

class Main extends Component {

    state = {
        items: []
    };

    componentDidMount() {
        axios.get('/products')
            .then(response => {
                this.setState({items: response.data});
            })
    }

    render() {

        const {items} = this.state;
        let itemList = null;

        if (items) {
            itemList = items.map(item => {
                return <div className="col-lg-3 col-md-6 mb-4 text-success" key={item.id}>
                    <div className="card h-100 borderr">
                        <img className="card-img-top img-fluid img-responsive"
                             src={`http://localhost:8080/api/products/${item.id}/image`}
                             style={{height: '160px'}} alt=""/>
                        <div className="card-body">
                            <h4 className="card-title">{item.name}</h4>
                            <p className="card-text">
                                {item.details}
                            </p>
                            <h4 className="card-title">Producer: {item.producer}</h4>
                            <h4 className="card-title">Category: {item.category}</h4>

                        </div>
                        <div className="card-footer">
                            <button className="btn btn-success">Find Out More!</button>
                        </div>
                    </div>
                </div>
            })
        }


        return (
            <Aux>
                <div className="container">
                    <header className="jumbotron my-4 text-success bg-light">
                        <img src={Organic} className="img-fluid"/>
                        <h1 className="display-3">Global Organic Market</h1>
                        <p className="lead">Welcome to the platform where every product is organic!</p>
                    </header>
                    <div className="row text-center">
                        {itemList}
                    </div>
                </div>
            </Aux>
        );
    }

}

export default Main;