import React, {Component} from 'react';
import Aux from '../util/Aux';
import axios from '../custom-axios';


class AllProducts extends Component {

    constructor() {
        super();

        this.state = {
            items: [],
            categories: [],
            selectedCategory: '---',
            searchTerm: ''
        };

    }


    componentDidMount() {
        axios.get('/products')
            .then(response => {
                this.setState({items: response.data});
            });

        axios.get('/category-types')
            .then(response => {
                this.setState({categories: response.data});
            });
    }

    onHandleSearch = (event) => {
        const newState = {...this.state};

        this.setState({
            ...newState,
            searchTerm: event.target.value.toLowerCase()
        });
    };

    onChangeCategory = (event) => {
        const newState = {...this.state};

        this.setState({
            ...newState,
            selectedCategory: event.target.value
        });
    };

    render() {

        const {items, searchTerm, selectedCategory, categories} = this.state;
        let itemList = [], allCategories = [];

        console.log(selectedCategory);
        if (categories) {
            allCategories = categories.map(c => {
                return <option key={c} name={c} value={c}>
                    {c}
                </option>
            })
        }
        if (items) {
            itemList = items.map(item => {
                if (item.name.toLowerCase().search(searchTerm) !== -1 && (item.category === selectedCategory || selectedCategory === '---')) {
                    return <div key={item.id}>
                        <hr/>
                        <br/>
                        <div className="row text-success borderr">
                            <div className="col-md-5">
                                <a href="#">
                                    <img className='img-fluid hheight'
                                         src={`http://localhost:8080/api/products/${item.id}/image`} alt=""/>
                                </a>
                            </div>
                            <div className="col-md-7">
                                <br/>
                                <h3>{item.name}</h3>
                                <p>{item.details}</p>
                                <h3>Producer: {item.producer}</h3>
                                <h3>Category: {item.category}</h3>
                                <button className="btn btn-success">View More</button>
                            </div>
                        </div>
                    </div>
                }
            })
        }

        return (
            <div className='container'>

                <div className='row' style={{marginTop: '10px'}}>
                    <div className="col-md-6">
                        <input className="form-control form-control-lg" type="text" placeholder="Search"
                               aria-label="Search" onChange={this.onHandleSearch}/>
                    </div>
                    {
                        categories && <div className="offset-md-1 col-md-4">
                            <select className="form-control" name="category" id="product1" value={selectedCategory}
                                    onChange={(event) => {
                                        this.onChangeCategory(event)
                                    }}>
                                <option id="default" name="default">---</option>
                                {allCategories}
                            </select>
                        </div>
                    }
                </div>

                {itemList}

                <hr/>

                <ul className="pagination justify-content-center">
                    <li className="page-item">
                        <a className="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span className="sr-only">Previous</span>
                        </a>
                    </li>
                    <li className="page-item">
                        <a className="page-link" href="#">1</a>
                    </li>
                    <li className="page-item">
                        <a className="page-link" href="#">2</a>
                    </li>
                    <li className="page-item">
                        <a className="page-link" href="#">3</a>
                    </li>
                    <li className="page-item">
                        <a className="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span className="sr-only">Next</span>
                        </a>
                    </li>
                </ul>
            </div>


        );

    }

}

export default AllProducts;